package com.systango.boilerplatecode.data.network;

import androidx.lifecycle.LiveData;

import com.systango.boilerplatecode.data.models.PostListModel;
import com.systango.boilerplatecode.data.models.multipartModel.ResponseMultipartModel;
import com.systango.boilerplatecode.utility.ApiResponse;

import okhttp3.MultipartBody;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiServices {

    @GET("679bc5cc-1ce9-41a4-971a-71910a24cbf1")
    LiveData<ApiResponse<PostListModel>> getEmployee();

    @Multipart
    @POST("post")
    LiveData<ApiResponse<ResponseMultipartModel>> updateProfile(@Part("user_id") String id,
                                                                @Part("full_name") String fullName,
                                                                @Part MultipartBody.Part image,
                                                                @Part("other") String other);

    @POST("post/asd")
    LiveData<ApiResponse<ResponseMultipartModel>> checkError();


}
