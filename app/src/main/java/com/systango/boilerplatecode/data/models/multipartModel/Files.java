
package com.systango.boilerplatecode.data.models.multipartModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Files {

    @SerializedName("ABc")
    @Expose
    private String aBc;

    public String getABc() {
        return aBc;
    }

    public void setABc(String aBc) {
        this.aBc = aBc;
    }

}
