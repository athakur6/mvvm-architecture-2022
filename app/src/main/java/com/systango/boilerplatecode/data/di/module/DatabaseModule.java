package com.systango.boilerplatecode.data.di.module;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.systango.boilerplatecode.data.database.AppDatabase;
import com.systango.boilerplatecode.data.database.dao.LoginDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.qualifiers.ApplicationContext;
import dagger.hilt.components.SingletonComponent;

// adding annotation for our database entities and db version.
@InstallIn(SingletonComponent.class)
@Module
public class DatabaseModule {

    @Provides
    public LoginDao getLoginDao(AppDatabase appDatabase) {
        return appDatabase.loginModel();
    }

    @Provides
    @Singleton
    public AppDatabase providerAppDatabase(@ApplicationContext Context context) {
        return Room.databaseBuilder(context,
                        AppDatabase.class,
                        context.getResources().getString(com.systango.boilerplatecode.R.string.app_name))
                .allowMainThreadQueries() .fallbackToDestructiveMigration()
                .addCallback(callback)
                .build();
    }

    public static RoomDatabase.Callback callback = new RoomDatabase.Callback() {
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
        }
    };
}
