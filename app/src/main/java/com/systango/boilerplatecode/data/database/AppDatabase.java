package com.systango.boilerplatecode.data.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.systango.boilerplatecode.data.database.dao.LoginDao;
import com.systango.boilerplatecode.data.database.model.LoginModel;

@Database(entities = {LoginModel.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract LoginDao loginModel();

}
