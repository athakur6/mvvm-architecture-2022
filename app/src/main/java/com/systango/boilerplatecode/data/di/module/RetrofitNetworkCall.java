package com.systango.boilerplatecode.data.di.module;

import com.systango.boilerplatecode.BuildConfig;
import com.systango.boilerplatecode.data.network.ApiMethodRetrofit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.components.SingletonComponent;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/*
* THIS FILE IS WITH NORMAL RXJAVA CALL WITHOUT COMMON RETROFIT CALLING
*/

@InstallIn(SingletonComponent.class)
@Module
public class RetrofitNetworkCall {

//    @Singleton
//    @Provides
    public Retrofit provideRetrofit() {
        return new Retrofit.Builder().baseUrl(BuildConfig.BASE_URL)
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Singleton
    @Provides
    public ApiMethodRetrofit providerRetrofitApi(Retrofit retrofit) {
        return retrofit.create(ApiMethodRetrofit.class);
    }
}
