package com.systango.boilerplatecode.data.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Update;

import com.systango.boilerplatecode.data.database.model.LoginModel;

@Dao
public interface LoginDao {

    // below method is use to
    // add data to database.
    @Insert
    Long insert(LoginModel model);

    // below method is use to update
    // the data in our database.
    @Update
    void update(LoginModel model);
}
