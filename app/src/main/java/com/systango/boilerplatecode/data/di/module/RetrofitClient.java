package com.systango.boilerplatecode.data.di.module;

import com.systango.boilerplatecode.BuildConfig;
import com.systango.boilerplatecode.data.network.ApiServices;
import com.systango.boilerplatecode.utility.Constant;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.components.SingletonComponent;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


@InstallIn(SingletonComponent.class)
@Module
public final class RetrofitClient {

    private static Retrofit instance;

    @Singleton
    @Provides
    public  Retrofit getInstance() {
        if (instance == null) {
            synchronized (RetrofitClient.class) {
                instance = new Retrofit.Builder()
                        .baseUrl(BuildConfig.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
//                        .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                        .client(getOkHttpClient())
                        .build();
            }
        }
        return instance;
    }

    private  OkHttpClient getOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
//        if (BuildConfig.DEBUG & RETROFIT_CALL_LOG_ENABLE) {
//            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
//            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
//            builder.addInterceptor(httpLoggingInterceptor);
//        }
        builder.readTimeout(Constant.NETWORK_CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS);
        builder.connectTimeout(Constant.NETWORK_READ_TIMEOUT, TimeUnit.MILLISECONDS);
        return builder.build();
    }


    @Singleton
    @Provides
    static ApiServices provideRetrofitService(Retrofit retrofit) {
        return retrofit.create(ApiServices.class);
    }


}
