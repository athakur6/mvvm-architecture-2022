
package com.systango.boilerplatecode.data.models.multipartModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Headers {

    @SerializedName("Accept")
    @Expose
    private String accept;
    @SerializedName("Accept-Encoding")
    @Expose
    private String acceptEncoding;
    @SerializedName("Content-Length")
    @Expose
    private String contentLength;
    @SerializedName("Content-Type")
    @Expose
    private String contentType;
    @SerializedName("Host")
    @Expose
    private String host;
    @SerializedName("Postman-Token")
    @Expose
    private String postmanToken;
    @SerializedName("User-Agent")
    @Expose
    private String userAgent;
    @SerializedName("X-Amzn-Trace-Id")
    @Expose
    private String xAmznTraceId;

    public String getAccept() {
        return accept;
    }

    public void setAccept(String accept) {
        this.accept = accept;
    }

    public String getAcceptEncoding() {
        return acceptEncoding;
    }

    public void setAcceptEncoding(String acceptEncoding) {
        this.acceptEncoding = acceptEncoding;
    }

    public String getContentLength() {
        return contentLength;
    }

    public void setContentLength(String contentLength) {
        this.contentLength = contentLength;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPostmanToken() {
        return postmanToken;
    }

    public void setPostmanToken(String postmanToken) {
        this.postmanToken = postmanToken;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getXAmznTraceId() {
        return xAmznTraceId;
    }

    public void setXAmznTraceId(String xAmznTraceId) {
        this.xAmznTraceId = xAmznTraceId;
    }

}
