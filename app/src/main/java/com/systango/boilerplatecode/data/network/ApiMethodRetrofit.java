package com.systango.boilerplatecode.data.network;

import com.systango.boilerplatecode.data.models.CommentListModel;
import com.systango.boilerplatecode.data.models.PostListModel;

import java.util.List;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiMethodRetrofit {

    @GET("posts")
    Observable<List<PostListModel>> getListOfPost();

    @GET("posts/{id}/comments")
    Observable<List<CommentListModel>> getCommentList(
            @Path("id") int id
    );


}
