package com.systango.boilerplatecode.data.network.response;

import com.google.gson.annotations.SerializedName;

public class ErrorResponse {

    @SerializedName("statusCode")
    public int statusCode;

    @SerializedName("message")
    public String message;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
