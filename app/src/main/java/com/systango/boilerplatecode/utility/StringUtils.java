package com.systango.boilerplatecode.utility;

import javax.annotation.Nullable;

public class StringUtils {


    /**
     * Checks given string obj is 
     * @param string
     * @return
     */
    private static boolean isNotNull(String string) {
        return string != null;
    }

    public static boolean isBlank(String string) {
        if (isNotNull(string)) {
            return string.trim().isEmpty();
        }
        return false;
    }

    public static String get(String string, @Nullable String defValue) {
        if (isNotNull(string)) {
            return string;
        } else {
            return defValue;
        }
    }

    public static String get(String string) {
        if (isNotNull(string)) {
            return string;
        } else {
            return "";
        }
    }
}
