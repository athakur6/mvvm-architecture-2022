package com.systango.boilerplatecode.utility;

/**
 * Status of a API Call that is provided to the UI.
 */
public enum ApiError {
    NETWORK,
    INVALID_INPUT,
    ERROR,
}