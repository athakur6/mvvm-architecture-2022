package com.systango.boilerplatecode.utility;

import android.content.Context;

import androidx.annotation.StringRes;

public class UiText {

    public static final class DynamicString extends UiText {

        private final String value;

        private DynamicString(String value) {
            this.value = value;
        }

    }

    public static final class StringResource extends UiText {

        @StringRes
        private final int resId;
        private final Object[] arg;


        private StringResource(@StringRes int resId, Object... arg) {
            this.resId = resId;
            this.arg = arg;
        }

    }


    public static UiText toUiText(String str) {
        return new DynamicString(str);
    }

    public static UiText toUiText(@StringRes int resId, Object... arg) {
        return new StringResource(resId, arg);
    }

    public String getValue(Context context) {
        if (this instanceof DynamicString) {
            return ((DynamicString) this).value;
        } else if (this instanceof StringResource) {
            return context.getString(((StringResource) this).resId, ((StringResource) this).arg);
        } else {
            return null;
        }
    }
}