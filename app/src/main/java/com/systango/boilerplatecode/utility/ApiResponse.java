package com.systango.boilerplatecode.utility;

import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.systango.boilerplatecode.R;
import com.systango.boilerplatecode.data.network.response.ErrorResponse;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit2.Response;

public final class ApiResponse<T> {

    public final ApiStatus apiStatus;

    public final int code;

    @Nullable
    public final T body;

    @Nullable
    public final UiText errorMessage;

    @Nullable
    public final ApiError apiError;

    public final int flag;

    private ApiResponse() {
        apiStatus = ApiStatus.LOADING;
        code = -1;
        body = null;
        errorMessage = null;
        apiError = null;
        flag = -1;
    }

    private ApiResponse(Response<T> response) {
        flag = -1;
        code = response.code();
        body = response.body();
        if (response.isSuccessful()) {
            apiError = null;
            apiStatus = ApiStatus.SUCCESS;
            errorMessage = null;
        } else {
            apiError = ApiError.ERROR;
            apiStatus = ApiStatus.ERROR;
            errorMessage = parseError(response);
        }
    }


    private ApiResponse(Throwable throwable) {
        apiStatus = ApiStatus.ERROR;
        body = null;
        code = -1;
        flag = -1;
        if (throwable instanceof UnknownHostException) {
            errorMessage = UiText.toUiText(R.string.No_internet_connection_Make_sure_Wi_Fi_or_cellular_data_is_turned_on_then_try_again);
            apiError = ApiError.NETWORK;
        } else if (throwable instanceof SocketTimeoutException) {
            errorMessage = UiText.toUiText(R.string.Connection_to_host_failed_Error_SocketTimeoutException);
            apiError = ApiError.ERROR;
        } else if (throwable instanceof JsonSyntaxException) {
            errorMessage = UiText.toUiText(R.string.Web_service_request_failed_Error_Description, "JsonSyntaxException", throwable.getMessage());
            apiError = ApiError.ERROR;
        } else {
            apiError = ApiError.ERROR;
            if (StringUtils.isBlank(throwable.getMessage())) {
                errorMessage = UiText.toUiText(R.string.Connectio_to_host_failed_Error_Unknown);
            } else {
                errorMessage = UiText.toUiText(throwable.getMessage());
            }
        }
    }


    private ApiResponse(ApiError apiError, UiText errorMessage, int flag) {
        apiStatus = ApiStatus.ERROR;
        code = -1;
        body = null;
        this.errorMessage = errorMessage;
        this.apiError = apiError;
        this.flag = flag;
    }


    private UiText parseError(Response<T> response) {
        ErrorResponse errorResponse = null;
        if (response.errorBody() != null) {
            try {
                errorResponse = new Gson().fromJson(response.errorBody().charStream(), ErrorResponse.class);
                return UiText.toUiText(errorResponse.getMessage());
            } catch (Exception e) {
            }
        }
        return UiText.toUiText(R.string.Web_service_request_failed_Error, response.code());
    }


    public static <T> ApiResponse<T> loading() {
        return new ApiResponse<>();
    }

    public static <T> ApiResponse<T> success(Response<T> response) {
        return new ApiResponse<>(response);
    }

    public static <T> ApiResponse<T> failure(Throwable throwable) {
        return new ApiResponse<>(throwable);
    }

    public static <T> ApiResponse<T> invalidInput(UiText error, int flag) {
        return new ApiResponse<>(ApiError.INVALID_INPUT, error, flag);
    }

    public static <T> ApiResponse<T> invalidInput(UiText error) {
        return new ApiResponse<>(ApiError.INVALID_INPUT, error, -1);
    }
}
