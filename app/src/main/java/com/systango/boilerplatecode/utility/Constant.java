package com.systango.boilerplatecode.utility;


import com.systango.boilerplatecode.BuildConfig;

public final class Constant {


    private Constant() {

    }

    public static long NETWORK_CONNECTION_TIMEOUT = 10 * 1000L; // in milliseconds, 0 means no time out
    public static long NETWORK_READ_TIMEOUT = 10 * 1000L; // in milliseconds, 0 means no time out
    public static boolean RETROFIT_CALL_LOG_ENABLE = true; // It will works only in debug type build
    public static int SPLASH_TIMEOUT = 3000; // It will works only in debug type build

    public static String getBaseURL() {
        if (BuildConfig.DEBUG) {
            return "https://mocki.io/v1/";
        } else {
            return "https://mocki.io/v1/";
        }
    }
}