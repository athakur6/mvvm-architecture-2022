package com.systango.boilerplatecode.views.Login;

import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.systango.boilerplatecode.data.database.dao.LoginDao;
import com.systango.boilerplatecode.data.database.model.LoginModel;

import org.json.JSONException;

import javax.inject.Inject;

public class LoginActivityRepository {
    private LoginDao loginDao;
    private CallbackManager callbackManager;
    private LoginManager loginManager;

    @Inject
    LoginActivityRepository(LoginDao loginDao) {
        this.loginDao = loginDao;

    }

    public void createUser(LoginModel model) {
        Runnable r = () -> Log.e(">>>>>>>>>", "run:>>>>>>>>>>>>>>>>>>> " + loginDao.insert(model));
        new Thread(r).start();
    }

    public void facebookLogin() {

        loginManager
                = LoginManager.getInstance();
        callbackManager
                = CallbackManager.Factory.create();

        loginManager
                .registerCallback(
                        callbackManager,
                        new FacebookCallback<LoginResult>() {

                            @Override
                            public void onSuccess(LoginResult loginResult) {
                                GraphRequest request = GraphRequest.newMeRequest(

                                        loginResult.getAccessToken(),

                                        (object, response) -> {

                                            if (object != null) {
                                                try {
                                                    String name = object.getString("name");
                                                    String email = object.getString("email");
                                                    String fbUserID = object.getString("id");

                                                    //disconnectFromFacebook();

                                                    // do action after Facebook login success
                                                    // or call your API
                                                } catch (JSONException | NullPointerException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                Bundle parameters = new Bundle();
                                parameters.putString(
                                        "fields",
                                        "id, name, email, gender, birthday");
                                request.setParameters(parameters);
                                request.executeAsync();
                            }

                            @Override
                            public void onCancel() {
                                Log.v("LoginScreen", "---onCancel");
                            }

                            @Override
                            public void onError(FacebookException error) {
                                // here write code when get error
                                Log.v("LoginScreen", "----onError: "
                                        + error.getMessage());
                            }
                        });
    }

    public void disconnectFromFacebook() {
        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }

        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me/permissions/",
                null,
                HttpMethod.DELETE,
                graphResponse -> LoginManager.getInstance().logOut())
                .executeAsync();
    }

}
