package com.systango.boilerplatecode.views.baseClasses;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Window;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewbinding.ViewBinding;

import com.systango.boilerplatecode.R;

public abstract class BaseActivity<BINDING extends ViewBinding, VM extends BaseViewModel> extends AppCompatActivity {

    protected VM activityViewModel;
    protected BINDING activityBinding;
    private Dialog progressDialogNew;

    @NonNull
    protected abstract VM createViewModel();

    @NonNull
    protected abstract BINDING createViewBinding(LayoutInflater layoutInflater);

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = createViewBinding(LayoutInflater.from(this));
        setContentView(activityBinding.getRoot());
        activityViewModel = createViewModel();
    }


    public void showHideProgressDialog(boolean isShow) {
        if (progressDialogNew != null) {
            if (isShow) {
                progressDialogNew.show();
            } else {
                progressDialogNew.dismiss();
            }
        } else {
            progressDialogNew = new Dialog(this);
            progressDialogNew.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progressDialogNew.setContentView(R.layout.progress_dialog_loading_custom);
            progressDialogNew.setCancelable(false);
            progressDialogNew.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            showHideProgressDialog(isShow);
        }
    }

}
