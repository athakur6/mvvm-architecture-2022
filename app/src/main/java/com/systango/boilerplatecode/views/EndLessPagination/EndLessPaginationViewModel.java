package com.systango.boilerplatecode.views.EndLessPagination;

import android.util.Log;

import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.systango.boilerplatecode.views.RecyclerView.PostAdapter;
import com.systango.boilerplatecode.data.models.CommentListModel;
import com.systango.boilerplatecode.data.models.PostListModel;
import com.systango.boilerplatecode.views.baseClasses.BaseViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;

@HiltViewModel
public class EndLessPaginationViewModel extends BaseViewModel {
    private final EndLessPaginationRepository endLessPaginationRepository;
    private ArrayList<PostListModel> postListModels;
    private PostAdapter postAdapter = null;

    @Inject
    EndLessPaginationViewModel(EndLessPaginationRepository endLessPaginationRepository) {
        this.endLessPaginationRepository = endLessPaginationRepository;
        postListModels = new ArrayList<>();
    }

    public PostAdapter getPostAdapter() {
        if (postAdapter == null) {
            postAdapter = new PostAdapter();
        }
        return postAdapter;
    }

    public Observable<List<PostListModel>> getPostList() {
        Log.e(".>>>>", "getPostList: >>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        return endLessPaginationRepository.getListOfPost();
    }

    public Observable<List<CommentListModel>> getCommentList(int post_id) {
        return endLessPaginationRepository.getListOfComments(post_id);
    }

    public Observable<PostListModel> getCommentObservable(PostListModel post) {
        return getCommentList(post.getId())
                .subscribeOn(Schedulers.io())
                .map(commentListModels -> {
                    int delay = (new Random().nextInt(5) + 1) * 1000;
                    Thread.sleep(delay);
                    post.setComments(commentListModels);
                    return post;
                });
    }

    public Observable<PostListModel> getPostListObservable() {
        return getPostList().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(postListModels1 -> {
                    getPostAdapter().updateList((ArrayList<PostListModel>) postListModels1);
                    int start = postListModels.size();
                    postListModels.addAll(postListModels1);
                    getPostAdapter().notifyItemRangeInserted(start, postListModels1.size());
                    Log.e(">>>>>>>>>>>>>>>>>", "getPostListObservable: " + new Gson().toJson(postListModels1));
                    //getPostAdapter().notifyAdapter(postListModels.size(), postListModels1.size() + postListModels.size());
                    return Observable.fromIterable(postListModels1)
                            .subscribeOn(Schedulers.io()
                            );
                });
    }
}

