package com.systango.boilerplatecode.views.RecyclerView;

import com.systango.boilerplatecode.data.models.CommentListModel;
import com.systango.boilerplatecode.data.models.PostListModel;
import com.systango.boilerplatecode.data.network.ApiMethodRetrofit;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.rxjava3.core.Observable;

public class RecyclerViewActivityRepository {
    private final ApiMethodRetrofit apiMethodRetrofit;

    @Inject
    RecyclerViewActivityRepository(ApiMethodRetrofit apiMethodRetrofit) {
        this.apiMethodRetrofit = apiMethodRetrofit;
    }

    public Observable<List<PostListModel>> getListOfPost() {
        return apiMethodRetrofit.getListOfPost();
    }

    public Observable<List<CommentListModel>> getListOfComments(int post_id) {
        return apiMethodRetrofit.getCommentList(post_id);
    }
}
