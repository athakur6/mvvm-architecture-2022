package com.systango.boilerplatecode.views.RecyclerView;

import androidx.lifecycle.ViewModel;

import com.systango.boilerplatecode.data.models.CommentListModel;
import com.systango.boilerplatecode.data.models.PostListModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;

@HiltViewModel
public class RecyclerViewActivityViewModel extends ViewModel {
    private final RecyclerViewActivityRepository recyclerViewActivityRepository;
    private ArrayList<PostListModel> postListModels;
    private PostAdapter postAdapter = null;

    @Inject
    RecyclerViewActivityViewModel(RecyclerViewActivityRepository recyclerViewActivityRepository) {
        this.recyclerViewActivityRepository = recyclerViewActivityRepository;
        postListModels = new ArrayList<>();
    }

    public PostAdapter getPostAdapter() {
        if (postAdapter == null) {
            postAdapter = new PostAdapter();
        }
        return postAdapter;
    }

    //get the post list
    public Observable<List<PostListModel>> getPostList() {
        return recyclerViewActivityRepository.getListOfPost();
    }

    //get the comments list based on post id
    public Observable<List<CommentListModel>> getCommentList(int post_id) {
        return recyclerViewActivityRepository.getListOfComments(post_id);
    }

    //Get and set the comments based on post in the list
    public Observable<PostListModel> getCommentObservable(PostListModel post) {
        return getCommentList(post.getId())
                .subscribeOn(Schedulers.io())
                .map(commentListModels -> {
                    int delay = (new Random().nextInt(5) + 1) * 1000;
                    Thread.sleep(delay);
                    post.setComments(commentListModels);
                    return post;
                });
    }

    //Get the post list and update the adapter
    public Observable<PostListModel> getPostListObservable() {
        return getPostList().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(postListModels1 -> {
                    getPostAdapter().updateList((ArrayList<PostListModel>) postListModels1);
                    postListModels.addAll(postListModels1);
                    return Observable.fromIterable(postListModels1)
                            .subscribeOn(Schedulers.io()
                            );
                });
    }
}
