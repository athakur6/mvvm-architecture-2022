package com.systango.boilerplatecode.views.Multipart;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewbinding.ViewBinding;

import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;

import com.systango.boilerplatecode.R;
import com.systango.boilerplatecode.databinding.ActivityMultipartCodeBinding;
import com.systango.boilerplatecode.views.baseClasses.BaseActivity;
import com.systango.boilerplatecode.views.baseClasses.BaseViewModel;

import java.io.IOException;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class MultipartActivity extends BaseActivity<ActivityMultipartCodeBinding,MultipartViewModel> {
    private ActivityMultipartCodeBinding multipartCodeBinding;
    private MultipartViewModel multipartViewModel;
    private ActivityResultLauncher<Intent> galleryActivityResult;


    @NonNull
    @Override
    protected MultipartViewModel createViewModel() {
        return null;
    }

    @NonNull
    @Override
    protected ActivityMultipartCodeBinding createViewBinding(LayoutInflater layoutInflater) {
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        multipartCodeBinding = DataBindingUtil.setContentView(this, R.layout.activity_multipart_code);

        multipartViewModel = new ViewModelProvider(this).get(MultipartViewModel.class);

        multipartCodeBinding.btnRequestMultipart.setOnClickListener(v -> {
            Intent i = new Intent();
            i.setType("image/*");
            i.setAction(Intent.ACTION_GET_CONTENT);

            galleryActivityResult.launch(i);

//            multipartViewModel.checkError(MultipartActivityCode.this);
        });

        galleryActivityResult = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                uri -> {
                    if (uri.getData() != null) {
                        try {
                            /*
                             * Get the image
                             * Convert to bitmap
                             * Save to cache
                             * Get the file object and send to multipart request
                             */
                            multipartViewModel.uploadImageWithMultipart(MultipartActivity.this, multipartViewModel.convertBitmapToFile("image1.jpg", MediaStore.Images.Media.getBitmap(
                                    this.getContentResolver(),
                                    uri.getData().getData()), MultipartActivity.this));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });


    }
}