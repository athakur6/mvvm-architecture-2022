package com.systango.boilerplatecode.views.RecyclerView;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewbinding.ViewBinding;

import android.os.Bundle;
import android.view.LayoutInflater;

import com.systango.boilerplatecode.R;
import com.systango.boilerplatecode.databinding.ActivityRecyclerViewBinding;
import com.systango.boilerplatecode.views.baseClasses.BaseActivity;
import com.systango.boilerplatecode.data.models.PostListModel;
import com.systango.boilerplatecode.views.baseClasses.BaseViewModel;

import dagger.hilt.android.AndroidEntryPoint;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

@AndroidEntryPoint
public class RecyclerViewActivity extends BaseActivity {

    private ActivityRecyclerViewBinding recyclerViewBinding;
    private RecyclerViewActivityViewModel recyclerViewActivityViewModel;
    CompositeDisposable compositeDisposable;

    @androidx.annotation.NonNull
    @Override
    protected BaseViewModel createViewModel() {
        return null;
    }

    @androidx.annotation.NonNull
    @Override
    protected ViewBinding createViewBinding(LayoutInflater layoutInflater) {
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        recyclerViewBinding = DataBindingUtil.setContentView(this, R.layout.activity_recycler_view);

        compositeDisposable = new CompositeDisposable();

        recyclerViewActivityViewModel = new ViewModelProvider(this).get(RecyclerViewActivityViewModel.class);

        recyclerViewBinding.rvPost.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        recyclerViewBinding.rvPost.setAdapter(recyclerViewActivityViewModel.getPostAdapter());

        recyclerViewActivityViewModel.getPostListObservable()
                .subscribeOn(Schedulers.io())
                .flatMap(it -> recyclerViewActivityViewModel.getCommentObservable(it))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PostListModel>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(@NonNull PostListModel postListModel) {
                        recyclerViewActivityViewModel.getPostAdapter().updatePost(postListModel);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.clear();
        super.onDestroy();
    }


}