package com.systango.boilerplatecode.views.Login;


import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

import static androidx.constraintlayout.helper.widget.MotionEffect.TAG;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.activity.result.ActivityResult;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.systango.boilerplatecode.R;
import com.systango.boilerplatecode.data.database.model.LoginModel;
import com.systango.boilerplatecode.utility.permission.PermissionUtils;
import com.systango.boilerplatecode.views.baseClasses.BaseViewModel;

import java.util.ArrayList;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class LoginActivityViewModel extends BaseViewModel {
    private final LoginActivityRepository loginActivityRepository;
    private PermissionUtils permissionUtils;
    private static final int REQUEST_CODE = 200;

    @Inject
    LoginActivityViewModel(LoginActivityRepository loginActivityRepository) {
        this.loginActivityRepository = loginActivityRepository;
    }

    public void createUser() {
        loginActivityRepository.createUser(new LoginModel("abc", "222222", "abc@gmail.com", "2134"));
    }

    public void allPermission(Context context) {
        permissionUtils = new PermissionUtils(context);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ArrayList<String> alForPermission = new ArrayList<>();
            alForPermission.add(READ_EXTERNAL_STORAGE);
            alForPermission.add(WRITE_EXTERNAL_STORAGE);
            permissionUtils.check_permission(alForPermission, context.getString(R.string.premission_des), REQUEST_CODE);
        }
    }

    public Intent googleSignIn(Context context) {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        GoogleSignInClient googleApiClient = GoogleSignIn.getClient(context, gso);
        return googleApiClient.getSignInIntent();
    }

    public void handleSignInResult(ActivityResult completedTask) {
        try {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(completedTask.getData());
            GoogleSignInAccount account = task.getResult(ApiException.class);
            // Signed in successfully

        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
        }
    }

    public void facebookLogin() {
        loginActivityRepository.facebookLogin();
    }


}
