package com.systango.boilerplatecode.views.EndLessPagination;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;

import android.os.Bundle;
import android.view.LayoutInflater;

import com.systango.boilerplatecode.R;
import com.systango.boilerplatecode.databinding.ActivityEndLessPaginationBinding;
import com.systango.boilerplatecode.views.baseClasses.BaseActivity;
import com.systango.boilerplatecode.data.models.PostListModel;
import com.systango.boilerplatecode.utility.EndlessRecyclerViewScrollListener;
import com.systango.boilerplatecode.views.baseClasses.BaseViewModel;

import dagger.hilt.android.AndroidEntryPoint;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

@AndroidEntryPoint
public class EndLessPaginationActivity extends BaseActivity<ActivityEndLessPaginationBinding, EndLessPaginationViewModel> {
    private CompositeDisposable compositeDisposable;

    // Store a member variable for the listener
    private EndlessRecyclerViewScrollListener scrollListener;

    @androidx.annotation.NonNull
    @Override
    protected EndLessPaginationViewModel createViewModel() {
        return new ViewModelProvider(this).get(EndLessPaginationViewModel.class);
    }

    @androidx.annotation.NonNull
    @Override
    protected ActivityEndLessPaginationBinding createViewBinding(LayoutInflater layoutInflater) {
        return DataBindingUtil.setContentView(this, R.layout.activity_end_less_pagination);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LinearLayoutManager lm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        activityBinding.rvEndlessList.setLayoutManager(lm);

        scrollListener = new EndlessRecyclerViewScrollListener(lm) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                getMoreData();
            }
        };

        // Adds the scroll listener to RecyclerView
        activityBinding.rvEndlessList.addOnScrollListener(scrollListener);

        compositeDisposable = new CompositeDisposable();

        activityBinding.rvEndlessList.setAdapter(activityViewModel.getPostAdapter());

        getMoreData();
    }

    void getMoreData() {
        activityViewModel.getPostListObservable()
                .subscribeOn(Schedulers.io())
                .flatMap(it -> activityViewModel.getCommentObservable(it))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PostListModel>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(@NonNull PostListModel postListModel) {
                        activityViewModel.getPostAdapter().updatePost(postListModel);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        compositeDisposable.clear();
                    }
                });
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.clear();
        super.onDestroy();
    }
}