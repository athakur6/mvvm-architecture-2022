package com.systango.boilerplatecode.views.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.systango.boilerplatecode.R;
import com.systango.boilerplatecode.databinding.PostAdapterBinding;
import com.systango.boilerplatecode.data.models.PostListModel;

import java.util.ArrayList;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder> {

    ArrayList<PostListModel> postListModels;

    public PostAdapter() {
        postListModels = new ArrayList<PostListModel>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_adapter, parent, false);
        PostAdapterBinding adapterBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.post_adapter, parent, false);

        return new ViewHolder(adapterBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PostListModel postListModels = this.postListModels.get(position);
        holder.adapterBinding.tvPostTitle.setText(postListModels.getTitle());
        if (postListModels.getComments() == null) {
            holder.adapterBinding.tpgWatingCommentCount.setVisibility(View.VISIBLE);
            holder.adapterBinding.tvPostCommentCount.setVisibility(View.GONE);
        } else {
            holder.adapterBinding.tvPostCommentCount.setVisibility(View.VISIBLE);
            holder.adapterBinding.tpgWatingCommentCount.setVisibility(View.GONE);
            holder.adapterBinding.tvPostCommentCount.setText("" + postListModels.getComments().size());
        }
    }

    public void updatePost(PostListModel postListModel) {
        int index = postListModels.indexOf(postListModel);
        postListModels.set(index, postListModel);
        notifyItemChanged(index);
    }

    public void updateList(ArrayList<PostListModel> listModel) {
        postListModels.addAll(listModel);
    }

    public void notifyAdapter(int start, int end){
        notifyItemRangeChanged(start, end);
    }

    @Override
    public int getItemCount() {
        return postListModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        PostAdapterBinding adapterBinding;

        public ViewHolder(@NonNull PostAdapterBinding itemView) {
            super(itemView.getRoot());
            adapterBinding = itemView;
        }
    }
}
