package com.systango.boilerplatecode.views.Multipart;

import androidx.lifecycle.LiveData;

import com.systango.boilerplatecode.data.models.multipartModel.ResponseMultipartModel;
import com.systango.boilerplatecode.data.network.ApiServices;
import com.systango.boilerplatecode.utility.ApiResponse;

import javax.inject.Inject;

import okhttp3.MultipartBody;

public class MultipartRepository {
    private final ApiServices apiServices;

    @Inject
    public MultipartRepository(ApiServices apiServices) {
        this.apiServices = apiServices;
    }

    public LiveData<ApiResponse<ResponseMultipartModel>> uploadImageViaMultipart(MultipartBody.Part body) {
        return apiServices.updateProfile("1231", "Systango", body, "");
    }

    public LiveData<ApiResponse<ResponseMultipartModel>> checkError() {
        return apiServices.checkError();
    }
}
