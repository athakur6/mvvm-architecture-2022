package com.systango.boilerplatecode.views.Login;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;

import com.systango.boilerplatecode.R;
import com.systango.boilerplatecode.databinding.ActivityLoginBinding;
import com.systango.boilerplatecode.views.baseClasses.BaseActivity;
import com.systango.boilerplatecode.views.DrawerNavigation.HomeNavigationActivity;
import com.systango.boilerplatecode.views.EndLessPagination.EndLessPaginationActivity;
import com.systango.boilerplatecode.views.Multipart.MultipartActivity;
import com.systango.boilerplatecode.views.RecyclerView.RecyclerViewActivity;
import com.systango.boilerplatecode.utility.permission.PermissionResultCallBack;
import com.systango.boilerplatecode.views.BottomWithDrawerNavigation.BottomNavigationActivity;

import java.util.ArrayList;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class LoginActivity extends BaseActivity<ActivityLoginBinding, LoginActivityViewModel> implements PermissionResultCallBack {
    private ActivityResultLauncher<Intent> someActivityResultLauncher;

    @NonNull
    @Override
    protected LoginActivityViewModel createViewModel() {
        return new ViewModelProvider(this).get(LoginActivityViewModel.class);
    }

    @NonNull
    @Override
    protected ActivityLoginBinding createViewBinding(LayoutInflater layoutInflater) {
        return DataBindingUtil.setContentView(this, R.layout.activity_login);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityViewModel.allPermission(this);

        activityBinding.btnInsertData.setOnClickListener(v -> activityViewModel.createUser());

        activityBinding.btnLogin.setOnClickListener(v -> startActivity(new Intent(this, MultipartActivity.class)));

        activityBinding.signInButton.setOnClickListener(v -> openSomeActivityForResult());

        activityBinding.btnfacebookLogin.setOnClickListener(v -> activityViewModel.facebookLogin());

        activityBinding.btnEndLess.setOnClickListener(v -> startActivity(new Intent(this, EndLessPaginationActivity.class)));

        activityBinding.btnRcycler.setOnClickListener(v -> startActivity(new Intent(this, RecyclerViewActivity.class)));

        activityBinding.btnDrawer.setOnClickListener(v -> startActivity(new Intent(this, HomeNavigationActivity.class)));

        activityBinding.btnBottomNavigation.setOnClickListener(v -> startActivity(new Intent(this, BottomNavigationActivity.class)));

        someActivityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        // The Task returned from this call is always completed, no need to attach
                        activityViewModel.handleSignInResult(result);
                    }
                });

    }

    public void openSomeActivityForResult() {
        //Create Intent
        Intent signInIntent = activityViewModel.googleSignIn(this);
        someActivityResultLauncher.launch(signInIntent);
    }

    @Override
    public void PermissionGranted(int request_code) {
        //Do the stuff
    }

    @Override
    public void PartialPermissionGranted(int request_code, ArrayList<String> granted_permissions) {
        //Do the stuff
    }

    @Override
    public void PermissionDenied(int request_code) {
        //Do the stuff
    }

    @Override
    public void NeverAskAgain(int request_code) {
        //Do the stuff
    }
}