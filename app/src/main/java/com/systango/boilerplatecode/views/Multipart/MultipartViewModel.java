package com.systango.boilerplatecode.views.Multipart;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.systango.boilerplatecode.views.baseClasses.BaseViewModel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

@HiltViewModel
public class MultipartViewModel extends BaseViewModel {
    private final MultipartRepository multipartRepository;

    @Inject
    public MultipartViewModel(MultipartRepository multipartRepository) {
        this.multipartRepository = multipartRepository;
    }

    //Here all the multipart logic written
    /*
     * Getting the path of the file from the URI, and creating the body of the request
     */
    public void uploadImageWithMultipart(Context context, File file) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
        // MultipartBody.Part is used to send also the actual filename
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        multipartRepository.uploadImageViaMultipart(body).observe((LifecycleOwner) context, apiResponse -> {
            Log.e(">>>>", new Gson().toJson(apiResponse));
        });
    }

    //Here converting the bitmap to file
    public File convertBitmapToFile(String fileName, Bitmap bitmap, Context context) throws IOException {
        //create a file to write bitmap data
        File file = new File(context.getCacheDir(), fileName);
        file.createNewFile();

        //Convert bitmap to byte array
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 0 /*ignored for PNG*/, bos);
        byte[] bitMapData = bos.toByteArray();

        //write the bytes in file
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            if (fos != null) {
                fos.write(bitMapData);
                fos.flush();
                fos.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

}
